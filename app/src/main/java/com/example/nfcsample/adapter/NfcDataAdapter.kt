package com.example.nfcsample.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nfcsample.R

class NfcDataAdapter(
    private val onMessageClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<NfcDataAdapter.NfcDataViewHolder>() {

    private val nfcMessages: MutableList<String> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NfcDataViewHolder =
        NfcDataViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.item_nfc_data,
                    parent,
                    false
                )
        )

    override fun onBindViewHolder(holder: NfcDataViewHolder, position: Int) {
        holder.onBind(
            position = position,
            message = nfcMessages[position],
            onMessageClickListener = onMessageClickListener
        )
    }

    override fun getItemCount(): Int = nfcMessages.size

    fun addMessage(message: String) {
        nfcMessages.add(message)
        notifyItemInserted(nfcMessages.lastIndex)
    }

    fun removeMessage(position: Int?): Boolean {
        if (position != null) {
            nfcMessages.removeAt(position)
            notifyItemRemoved(position)
        }

        return nfcMessages.isNotEmpty()
    }

    fun getMessages() = nfcMessages

    class NfcDataViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {

        fun onBind(
            position: Int,
            message: String,
            onMessageClickListener: (position: Int) -> Unit
        ) {
            val context = itemView.context

            itemView.findViewById<AppCompatTextView>(R.id.tv_message_position).apply {
                text = context.getString(R.string.title_message_position, correctPosition(position))
            }
            itemView.findViewById<AppCompatTextView>(R.id.tv_message_content).apply {
                text = context.getString(R.string.title_message_content, message)
            }

            itemView.rootView.setOnClickListener {
                onMessageClickListener.invoke(position)
            }
        }

        private fun correctPosition(position: Int) = (position + 1).toString()

    }

}