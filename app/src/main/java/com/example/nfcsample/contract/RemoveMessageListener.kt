package com.example.nfcsample.contract

interface RemoveMessageListener {

    fun onRemoveMessage(position: Int?)

}