package com.example.nfcsample.contract

interface MessageCollector {

    fun collectMessage(message: String)

}