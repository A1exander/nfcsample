package com.example.nfcsample.contract

import com.example.nfcsample.models.presentation.NfcDataModel

interface ReadNfcMessageCallback {

    fun onSuccess(nfcDataModel: NfcDataModel)

    fun onFailure(e: Exception)

}