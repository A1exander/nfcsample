package com.example.nfcsample.contract

interface WriteNfcMessageCallback {

    fun onProgress()

    fun onSuccess()

    fun onFailure(exception: Exception)

}