package com.example.nfcsample.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import com.example.nfcsample.R
import com.example.nfcsample.dialog.NfcWriteBottomSheet
import com.example.nfcsample.dialog.NfcWriteBottomSheet.Companion.TAG
import com.example.nfcsample.utils.NfcHelper
import com.example.nfcsample.utils.mapper.NfcMessageMapper

class SelectionActivity : AppCompatActivity() {

    private lateinit var btnWrite: AppCompatButton
    private lateinit var btnRead: AppCompatButton
    private lateinit var btnClear: AppCompatButton

    private lateinit var errorTv: AppCompatTextView

    private lateinit var navigationContainer: LinearLayout

    private lateinit var nfcHelper: NfcHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selection)

        initViews()
    }

    override fun onResume() {
        super.onResume()
        initNfcHelper()
        configureViews()
    }

    override fun onPause() {
        btnWrite.setOnClickListener(null)
        btnRead.setOnClickListener(null)
        btnClear.setOnClickListener(null)
        super.onPause()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        (supportFragmentManager.findFragmentByTag(TAG) as? NfcWriteBottomSheet)
            ?.onNewIntent(intent)
    }

    private fun initNfcHelper() {
        if (!::nfcHelper.isInitialized) {
            nfcHelper = NfcHelper(this)
        }
    }

    private fun initViews() {
        btnWrite = findViewById(R.id.btn_write_data)
        btnRead = findViewById(R.id.btn_read_data)
        btnClear = findViewById(R.id.btn_clear_tag)

        navigationContainer = findViewById(R.id.ll_navigation_container)
        errorTv = findViewById(R.id.tv_error)
    }

    private fun configureViews() {
        nfcHelper.errorText?.let {
            errorTv.text = it
        }

        errorTv.isVisible = !nfcHelper.isNfcEnabled
        navigationContainer.isVisible = nfcHelper.isNfcEnabled

        btnWrite.setOnClickListener { navigateTo(WriteDataActivity::class.java) }
        btnRead.setOnClickListener { navigateTo(ReadDataActivity::class.java) }
        btnClear.setOnClickListener {
            NfcWriteBottomSheet.showBottomSheet(
                nfcModel = NfcMessageMapper.createEmptyMessage(),
                fragmentManager = supportFragmentManager
            )
        }
    }

    private fun navigateTo(destinationClass: Class<out Activity>) {
        val intent = Intent(this, destinationClass)
        startActivity(intent)
    }

}