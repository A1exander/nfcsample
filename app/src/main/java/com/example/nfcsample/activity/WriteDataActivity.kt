package com.example.nfcsample.activity

import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nfcsample.R
import com.example.nfcsample.adapter.NfcDataAdapter
import com.example.nfcsample.contract.MessageCollector
import com.example.nfcsample.contract.RemoveMessageListener
import com.example.nfcsample.dialog.NfcDataDialogFragment
import com.example.nfcsample.dialog.NfcWriteBottomSheet
import com.example.nfcsample.dialog.RemoveMessageDialogFragment
import com.example.nfcsample.utils.mapper.NfcMessageMapper

class WriteDataActivity : AppCompatActivity(), MessageCollector, RemoveMessageListener {

    private lateinit var rvData: RecyclerView
    private lateinit var btnAddData: AppCompatButton
    private lateinit var btnWriteData: AppCompatButton

    private val nfcDataAdapter: NfcDataAdapter = NfcDataAdapter(
        onMessageClickListener = {
            RemoveMessageDialogFragment.showDialog(
                messagePosition = it,
                fragmentManager = supportFragmentManager
            )
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_data)

        initViews()
    }

    override fun onResume() {
        super.onResume()
        configureViews()
    }

    override fun onPause() {
        btnAddData.setOnClickListener(null)
        btnWriteData.setOnClickListener(null)
        super.onPause()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val fragment = supportFragmentManager
            .findFragmentByTag(NfcWriteBottomSheet.TAG)
        (fragment as? NfcWriteBottomSheet)?.onNewIntent(intent)
    }

    override fun collectMessage(message: String) {
        nfcDataAdapter.addMessage(message)
        if (!btnWriteData.isEnabled) {
            btnWriteData.isEnabled = true
        }
    }

    override fun onRemoveMessage(position: Int?) {
        val isWriteEnabled = nfcDataAdapter.removeMessage(position)
        btnWriteData.isEnabled = isWriteEnabled
    }

    private fun initViews() {
        rvData = findViewById(R.id.rv_data)
        btnAddData = findViewById(R.id.btn_add_data)
        btnWriteData = findViewById(R.id.btn_write_data)
    }

    private fun configureViews() {
        rvData.apply {
            adapter = nfcDataAdapter
            layoutManager = LinearLayoutManager(this@WriteDataActivity)
            addItemDecoration(
                DividerItemDecoration(this@WriteDataActivity, LinearLayout.VERTICAL)
            )
        }

        btnAddData.setOnClickListener {
            NfcDataDialogFragment.showDialog(supportFragmentManager)
        }

        btnWriteData.setOnClickListener {
            NfcWriteBottomSheet.showBottomSheet(
                nfcModel = NfcMessageMapper.createTextMessages(nfcDataAdapter.getMessages()),
                fragmentManager = supportFragmentManager
            )
        }
    }

}