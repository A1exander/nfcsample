package com.example.nfcsample.activity

import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import com.example.nfcsample.R
import com.example.nfcsample.contract.ReadNfcMessageCallback
import com.example.nfcsample.models.presentation.NfcDataModel
import com.example.nfcsample.utils.ForegroundNfcHelper
import com.example.nfcsample.utils.exception.EmptyTagException
import com.example.nfcsample.utils.ext.getCorrectParcelableExtra
import com.example.nfcsample.utils.parser.NdefParser

class ReadDataActivity : AppCompatActivity(), ReadNfcMessageCallback {

    private lateinit var tvHint: AppCompatTextView
    private lateinit var tvTagContent: AppCompatTextView

    private lateinit var foregroundNfcHelper: ForegroundNfcHelper

    private val ndefParser: NdefParser = NdefParser()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_data)

        initViews()
    }

    override fun onResume() {
        super.onResume()
        initForegroundNfcHelper()
        lifecycle.addObserver(foregroundNfcHelper)
        ndefParser.setMessageCallback(this)
        configureViews()
        parseNfcIntent(intent)
    }

    override fun onPause() {
        lifecycle.removeObserver(foregroundNfcHelper)
        ndefParser.setMessageCallback(null)
        super.onPause()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        parseNfcIntent(intent)
    }

    override fun onSuccess(nfcDataModel: NfcDataModel) {
        showTagData(nfcDataModel)
    }

    override fun onFailure(e: Exception) {
        showErrorText(true)
        tvHint.text = if (e is EmptyTagException) e.message
        else getString(R.string.title_tag_reading_failure, e.message)
    }

    private fun parseNfcIntent(intent: Intent?) {
        when (intent?.action) {
            NfcAdapter.ACTION_NDEF_DISCOVERED -> {
                val tag = intent.getCorrectParcelableExtra(NfcAdapter.EXTRA_TAG, Tag::class.java)
                ndefParser.parseNdefMessages(tag)
            }

            NfcAdapter.ACTION_TECH_DISCOVERED -> {
                val tag = intent.getCorrectParcelableExtra(NfcAdapter.EXTRA_TAG, Tag::class.java)
                val ndefTag: Ndef? = Ndef.get(tag)
                if (ndefTag == null) {
                    tvHint.text = getString(R.string.title_tag_empty)
                    showErrorText(true)
                } else ndefParser.parseNdefMessages(tag)
            }
        }
    }

    private fun initForegroundNfcHelper() {
        if (::foregroundNfcHelper.isInitialized.not()) {
            foregroundNfcHelper = ForegroundNfcHelper(this)
        }
    }

    private fun initViews() {
        tvHint = findViewById(R.id.tv_hint)
        tvTagContent = findViewById(R.id.tv_tag_content)
    }

    private fun configureViews() {
        tvHint.text = foregroundNfcHelper.errorText ?: getString(R.string.title_scan_tag)
        showErrorText(true)
    }

    private fun showTagData(nfcDataModel: NfcDataModel) {
        showErrorText(false)
        tvTagContent.text = getString(
            R.string.title_tag_content,
            nfcDataModel.tagTechnologies,
            nfcDataModel.dataSize,
            nfcDataModel.tagSize,
            getString(if (nfcDataModel.isEditable) R.string.title_yes else R.string.title_no),
            nfcDataModel.payload
        )
    }

    private fun showErrorText(shouldShow: Boolean) {
        tvHint.isVisible = shouldShow
        tvTagContent.isVisible = !shouldShow
    }

}