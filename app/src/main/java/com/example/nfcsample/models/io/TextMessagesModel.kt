package com.example.nfcsample.models.io

import android.os.Parcelable
import com.example.nfcsample.utils.FailureState
import com.example.nfcsample.utils.ProgressState
import com.example.nfcsample.utils.StartState
import com.example.nfcsample.utils.SuccessState
import kotlinx.parcelize.Parcelize

@Parcelize
data class TextMessagesModel(
    val data: List<String>,
    override val startState: StartState,
    override val progressState: ProgressState,
    override val successState: SuccessState,
    override val failureState: FailureState
) : NfcModel(
    startState = startState,
    progressState = progressState,
    successState = successState,
    failureState = failureState
), Parcelable