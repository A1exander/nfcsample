package com.example.nfcsample.models.presentation

data class NfcDataModel(
    val tagTechnologies: String,
    val tagSize: String,
    val dataSize: String,
    val isEditable: Boolean,
    val payload: String
)