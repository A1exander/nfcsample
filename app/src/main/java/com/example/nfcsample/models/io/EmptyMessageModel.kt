package com.example.nfcsample.models.io

import android.nfc.NdefRecord
import android.os.Parcelable
import com.example.nfcsample.utils.FailureState
import com.example.nfcsample.utils.ProgressState
import com.example.nfcsample.utils.StartState
import com.example.nfcsample.utils.SuccessState
import kotlinx.parcelize.Parcelize

@Parcelize
data class EmptyMessageModel(
    val data: Short = NdefRecord.TNF_EMPTY,
    override val startState: StartState,
    override val progressState: ProgressState,
    override val successState: SuccessState,
    override val failureState: FailureState
) : NfcModel(
    startState = startState,
    progressState = progressState,
    successState = successState,
    failureState = failureState
), Parcelable