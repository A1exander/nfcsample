package com.example.nfcsample.models.io

import android.os.Parcelable
import com.example.nfcsample.utils.FailureState
import com.example.nfcsample.utils.ProgressState
import com.example.nfcsample.utils.StartState
import com.example.nfcsample.utils.SuccessState
import kotlinx.parcelize.Parcelize

@Parcelize
sealed class NfcModel(
    open val startState: StartState,
    open val progressState: ProgressState,
    open val successState: SuccessState,
    open val failureState: FailureState
) : Parcelable