package com.example.nfcsample.utils.mapper

import android.nfc.NdefRecord
import com.example.nfcsample.models.io.EmptyMessageModel
import com.example.nfcsample.models.io.NfcModel
import com.example.nfcsample.models.io.TextMessagesModel
import com.example.nfcsample.utils.MIME_TEXT

object NdefMapper {

    fun map(nfcModel: NfcModel): List<NdefRecord> = when (nfcModel) {
        is TextMessagesModel -> mapTextMessages(nfcModel.data)
        is EmptyMessageModel -> mapEmptyMessage(nfcModel.data)
    }

    private fun mapTextMessages(messages: List<String>): List<NdefRecord> = messages.map {
        NdefRecord.createMime(
            MIME_TEXT,
            it.toByteArray()
        )
    }

    private fun mapEmptyMessage(tnf: Short): List<NdefRecord> = listOf(
        NdefRecord(
            tnf,
            null,
            null,
            null
        )
    )

}