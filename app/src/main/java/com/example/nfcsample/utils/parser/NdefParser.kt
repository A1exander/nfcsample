package com.example.nfcsample.utils.parser

import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import com.example.nfcsample.contract.ReadNfcMessageCallback
import com.example.nfcsample.utils.exception.EmptyTagException
import com.example.nfcsample.utils.exception.TagDisconnectedException
import com.example.nfcsample.utils.exception.UnsupportedTagTypeException
import com.example.nfcsample.utils.mapper.FromTagMapper
import com.example.nfcsample.utils.mapper.FromTagMapper.NEW_LINE_DELIMITER
import com.example.nfcsample.utils.withMainContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class NdefParser {

    private var messageCallback: ReadNfcMessageCallback? = null

    fun setMessageCallback(messageCallback: ReadNfcMessageCallback?) {
        this.messageCallback = messageCallback
    }

    fun parseNdefMessages(tag: Tag?) {
        val techList = tag?.techList

        val ndefTag = Ndef.get(tag)
        if (ndefTag == null) {
            val isFormatted = tryToFormatTag(
                tag = tag
            )

            if (!isFormatted) {
                messageCallback?.onFailure(UnsupportedTagTypeException())
            }

            return
        }

        CoroutineScope(Dispatchers.Main).launch {
            try {
                ndefTag.connect()
                if (!ndefTag.isConnected) {
                    withMainContext { messageCallback?.onFailure(TagDisconnectedException()) }
                    return@launch
                }

                val nfcDataModel = FromTagMapper.map(
                    techList = techList,
                    ndefTag = ndefTag
                )

                withMainContext {
                    if (nfcDataModel.payload.isEmpty() || nfcDataModel.payload == NEW_LINE_DELIMITER) {
                        messageCallback?.onFailure(EmptyTagException())
                    } else messageCallback?.onSuccess(nfcDataModel)
                }
            } catch (e: Exception) {
                withMainContext { messageCallback?.onFailure(e) }
            } finally {
                ndefTag.close()
            }
        }
    }

    private fun tryToFormatTag(tag: Tag?): Boolean {
        val ndefFormatable: NdefFormatable = NdefFormatable.get(tag) ?: return false

        try {
            CoroutineScope(Dispatchers.IO).launch {
                ndefFormatable.connect()
                if (!ndefFormatable.isConnected) {
                    cancel()
                }

                ndefFormatable.close()
            }
        } catch (e: Exception) {
            messageCallback?.onFailure(e)
        }

        return true
    }

}