package com.example.nfcsample.utils.mapper

import android.nfc.tech.Ndef
import com.example.nfcsample.models.presentation.NfcDataModel

object FromTagMapper {

    private const val COMMA_DELIMITER = ", "
    const val NEW_LINE_DELIMITER = "\n\t\t"

    fun map(
        techList: Array<out String>?,
        ndefTag: Ndef
    ): NfcDataModel {
        val isTagEditable = ndefTag.isWritable
        val maxSize = ndefTag.maxSize
        val dataSize = ndefTag.ndefMessage.byteArrayLength
        val ndefStringMessages = ndefTag.ndefMessage
            .records
            .map { String(it.payload) }

        val payload = mapListToString(
            payload = ndefStringMessages,
            delimiter = NEW_LINE_DELIMITER
        )

        val technologies = mapListToString(
            payload = techList
                ?.toList()
                .orEmpty()
                .map {
                    it.split(".")
                        .last()
                },
            delimiter = COMMA_DELIMITER
        )

        return NfcDataModel(
            tagTechnologies = technologies,
            tagSize = maxSize.toString(),
            dataSize = dataSize.toString(),
            isEditable = isTagEditable,
            payload = NEW_LINE_DELIMITER + payload
        )
    }

    private fun mapListToString(
        payload: List<String>,
        delimiter: String
    ): String {
        val builder = StringBuilder()
        payload.forEachIndexed { index, tech ->
            builder.append(tech)
            if (index != payload.lastIndex) {
                builder.append(delimiter)
            }
        }

        return builder.toString()
    }

}