package com.example.nfcsample.utils

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.nfc.tech.MifareUltralight
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.nfc.tech.NfcA
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.example.nfcsample.dialog.NfcWriteBottomSheet

class ForegroundNfcHelper(
    private val activity: Activity
) : NfcHelper(activity), DefaultLifecycleObserver {

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        nfcAdapter?.enableForegroundDispatch(
            activity,
            createPendingIntent(),
            arrayOf(
                IntentFilter(
                    NfcAdapter.ACTION_NDEF_DISCOVERED
                ).apply {
                    addDataType(MIME_TEXT)
                },
                IntentFilter(
                    NfcAdapter.ACTION_TECH_DISCOVERED
                )
            ),
            arrayOf(
                arrayOf<String>(
                    Ndef::class.java.name
                ),
                arrayOf<String>(
                    NfcA::class.java.name
                ),
                arrayOf<String>(
                    NdefFormatable::class.java.name
                ),
                arrayOf<String>(
                    MifareUltralight::class.java.name
                )
            )
        )
    }

    override fun onPause(owner: LifecycleOwner) {
        nfcAdapter?.disableForegroundDispatch(activity)
        super.onPause(owner)
    }

    private fun createPendingIntent(): PendingIntent {
        return PendingIntent.getActivity(
            activity,
            NfcWriteBottomSheet.REQUEST_CODE,
            createIntent(),
            PendingIntent.FLAG_MUTABLE
        )
    }

    private fun createIntent() = Intent(activity, activity::class.java).apply {
        addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
    }
}