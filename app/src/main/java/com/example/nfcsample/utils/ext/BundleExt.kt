package com.example.nfcsample.utils.ext

import android.os.Build
import android.os.Bundle

fun <T> Bundle?.getCorrectParcelable(name: String?, clazz: Class<T>): T? =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        this?.getParcelable(name, clazz)
    } else {
        this?.getParcelable(name)
    }