package com.example.nfcsample.utils

import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import com.example.nfcsample.contract.WriteNfcMessageCallback
import com.example.nfcsample.models.io.EmptyMessageModel
import com.example.nfcsample.models.io.NfcModel
import com.example.nfcsample.utils.exception.TagDisconnectedException
import com.example.nfcsample.utils.exception.TooLongMessageException
import com.example.nfcsample.utils.exception.UnsupportedTagTypeException
import com.example.nfcsample.utils.mapper.NdefMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MessageWriter {

    private var messageCallback: WriteNfcMessageCallback? = null

    fun setMessageListener(nfcMessageCallback: WriteNfcMessageCallback?) {
        messageCallback = nfcMessageCallback
    }

    fun writeMessagesToTag(nfcModel: NfcModel, tag: Tag) {
        val ndefTag: Ndef? = Ndef.get(tag)
        val ndefRecords = NdefMapper.map(nfcModel)
            .toTypedArray()

        if (ndefTag == null) {
            val isFormatted = tryToFormatTag(
                ndefRecords = ndefRecords,
                tag = tag
            )
            if (!isFormatted) {
                messageCallback?.onFailure(UnsupportedTagTypeException())
            }
            return
        }

        writeMessages(
            ndefTag = ndefTag,
            nfcModel = nfcModel,
            ndefRecords = ndefRecords
        )
    }

    private fun writeMessages(ndefTag: Ndef, nfcModel: NfcModel, ndefRecords: Array<NdefRecord>) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ndefTag.connect()
                if (!ndefTag.isWritable) {
                    return@launch
                }

                val finalRecords = detectCurrentTagMessages(
                    nfcModel = nfcModel,
                    tag = ndefTag,
                    newRecords = ndefRecords
                )

                val message = NdefMessage(finalRecords)

                if (ndefTag.maxSize < message.byteArrayLength) {
                    withMainContext { messageCallback?.onFailure(TooLongMessageException()) }
                    return@launch
                }

                if (ndefTag.isConnected) {
                    withMainContext { messageCallback?.onProgress() }
                    ndefTag.writeNdefMessage(message)
                    withMainContext { messageCallback?.onSuccess() }
                } else {
                    withMainContext { messageCallback?.onFailure(TagDisconnectedException()) }
                }
            } catch (e: Exception) {
                withMainContext { messageCallback?.onFailure(e) }
            } finally {
                ndefTag.close()
            }
        }
    }

    private fun detectCurrentTagMessages(
        nfcModel: NfcModel,
        tag: Ndef,
        newRecords: Array<NdefRecord>
    ): Array<NdefRecord> {
        val currentMessages = if (nfcModel is EmptyMessageModel) emptyArray()
        else tag.ndefMessage.records

        return currentMessages
            .filter { it.tnf != NdefRecord.TNF_EMPTY }
            .toMutableList()
            .apply { addAll(newRecords) }
            .toTypedArray()
    }

    private fun tryToFormatTag(ndefRecords: Array<NdefRecord>, tag: Tag): Boolean {
        val ndefFormatable: NdefFormatable = NdefFormatable.get(tag) ?: return false

        try {
            CoroutineScope(Dispatchers.IO).launch {
                ndefFormatable.connect()
                if (!ndefFormatable.isConnected) {
                    cancel()
                }

                ndefFormatable.format(
                    NdefMessage(ndefRecords)
                )

                ndefFormatable.close()
            }
        } catch (e: Exception) {
            messageCallback?.onFailure(e)
        }

        return true
    }

}