package com.example.nfcsample.utils

import android.os.Parcelable
import androidx.annotation.StringRes
import kotlinx.parcelize.Parcelize

@Parcelize
sealed class NfcSheetState : Parcelable

class StartState(@StringRes val title: Int) : NfcSheetState()

class ProgressState(@StringRes val title: Int) : NfcSheetState()

class SuccessState(@StringRes val title: Int) : NfcSheetState()

data class FailureState(
    @StringRes val title: Int,
    var exception: Exception = Exception()
) : NfcSheetState()