package com.example.nfcsample.utils

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun withMainContext(block: () -> Unit) {
    withContext(Dispatchers.Main) {
        block.invoke()
    }
}