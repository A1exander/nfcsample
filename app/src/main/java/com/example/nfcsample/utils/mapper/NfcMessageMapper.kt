package com.example.nfcsample.utils.mapper

import com.example.nfcsample.R
import com.example.nfcsample.models.io.EmptyMessageModel
import com.example.nfcsample.models.io.NfcModel
import com.example.nfcsample.models.io.TextMessagesModel
import com.example.nfcsample.utils.FailureState
import com.example.nfcsample.utils.ProgressState
import com.example.nfcsample.utils.StartState
import com.example.nfcsample.utils.SuccessState

object NfcMessageMapper {

    fun createEmptyMessage(): NfcModel = EmptyMessageModel(
        startState = StartState(R.string.title_find_tag),
        progressState = ProgressState(R.string.title_messages_writing),
        successState = SuccessState(R.string.title_successful_clearing),
        failureState = FailureState(R.string.title_clear_fail)
    )

    fun createTextMessages(messages: List<String>): NfcModel = TextMessagesModel(
        data = messages,
        startState = StartState(R.string.title_move_phone_to_tag),
        progressState = ProgressState(R.string.title_tag_clearing),
        successState = SuccessState(R.string.title_messages_successfully_write),
        failureState = FailureState(R.string.title_write_failure)
    )

}