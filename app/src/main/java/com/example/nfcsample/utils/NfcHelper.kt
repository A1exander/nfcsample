package com.example.nfcsample.utils

import android.content.Context
import android.nfc.NfcAdapter
import com.example.nfcsample.R

open class NfcHelper(private val context: Context) {

    private val adapter: NfcAdapter? = NfcAdapter.getDefaultAdapter(context)

    val isNfcEnabled: Boolean
        get() = adapter != null && adapter.isEnabled

    protected val nfcAdapter: NfcAdapter?
        get() {
            return if (isNfcEnabled) adapter else null
        }

    val errorText: String?
        get() = when {
            adapter == null -> context.getString(R.string.message_unavailable_nfc)
            !adapter.isEnabled -> context.getString(R.string.message_nfc_disabled)
            else -> null
        }

}