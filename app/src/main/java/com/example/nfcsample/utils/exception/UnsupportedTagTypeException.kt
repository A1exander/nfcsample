package com.example.nfcsample.utils.exception

class UnsupportedTagTypeException : Exception("Неподдержвиаемый тип метки")