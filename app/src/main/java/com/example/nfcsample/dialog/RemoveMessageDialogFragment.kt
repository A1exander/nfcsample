package com.example.nfcsample.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.nfcsample.R
import com.example.nfcsample.contract.RemoveMessageListener

class RemoveMessageDialogFragment : DialogFragment() {

    companion object {
        private const val MESSAGE_POSITION = "MESSAGE_POSITION"
        private val TAG = this::class.java.name

        fun showDialog(messagePosition: Int, fragmentManager: FragmentManager) {
            RemoveMessageDialogFragment().apply {
                arguments = bundleOf(MESSAGE_POSITION to messagePosition)
                show(fragmentManager, TAG)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.dialog_remove_message,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val position = arguments?.getInt(MESSAGE_POSITION)

        view.findViewById<AppCompatButton>(R.id.btn_remove).setOnClickListener {
            (context as? RemoveMessageListener)
                ?.onRemoveMessage(position)
            dismiss()
        }
    }

}