package com.example.nfcsample.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.nfcsample.R
import com.example.nfcsample.contract.MessageCollector

class NfcDataDialogFragment : DialogFragment() {

    companion object {
        fun showDialog(fragmentManager: FragmentManager) {
            NfcDataDialogFragment()
                .show(fragmentManager, null)
        }
    }

    private lateinit var etMessage: AppCompatEditText

    private lateinit var btnSubmit: AppCompatButton
    private lateinit var btnDecline: AppCompatButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.dialog_nfc_data,
        container,
        false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)
    }

    override fun onResume() {
        super.onResume()
        configureViews()
    }

    private fun initViews(view: View) {
        view.apply {
            etMessage = findViewById(R.id.et_message)

            btnSubmit = findViewById(R.id.btn_submit)
            btnDecline = findViewById(R.id.btn_decline)
        }
    }

    private fun configureViews() {
        showSoftKeyboard(etMessage)

        btnSubmit.setOnClickListener {
            (requireContext() as? MessageCollector)
                ?.collectMessage(
                    message = etMessage.text.toString()
                )
            this@NfcDataDialogFragment.dismiss()
        }

        btnDecline.setOnClickListener {
            this@NfcDataDialogFragment.dismiss()
        }
    }

    private fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = requireActivity().getSystemService(InputMethodManager::class.java)
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

}