package com.example.nfcsample.dialog

import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.Group
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import com.example.nfcsample.R
import com.example.nfcsample.contract.WriteNfcMessageCallback
import com.example.nfcsample.models.io.NfcModel
import com.example.nfcsample.utils.FailureState
import com.example.nfcsample.utils.ForegroundNfcHelper
import com.example.nfcsample.utils.MessageWriter
import com.example.nfcsample.utils.NfcSheetState
import com.example.nfcsample.utils.ProgressState
import com.example.nfcsample.utils.StartState
import com.example.nfcsample.utils.SuccessState
import com.example.nfcsample.utils.ext.getCorrectParcelable
import com.example.nfcsample.utils.ext.getCorrectParcelableExtra
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class NfcWriteBottomSheet : BottomSheetDialogFragment(), WriteNfcMessageCallback {

    companion object {
        val TAG: String = this::class.java.name
        const val REQUEST_CODE = 123
        private const val MESSAGES_EXTRA = "MESSAGES_EXTRA"

        fun showBottomSheet(
            nfcModel: NfcModel,
            fragmentManager: FragmentManager
        ) {
            NfcWriteBottomSheet()
                .apply {
                    arguments = bundleOf(MESSAGES_EXTRA to nfcModel)
                    show(fragmentManager, TAG)
                }
        }
    }

    private val messageWriter = MessageWriter()

    private var nfcModel: NfcModel? = null

    private lateinit var startDescription: AppCompatTextView
    private lateinit var successDescription: AppCompatTextView
    private lateinit var progressDescription: AppCompatTextView
    private lateinit var failureDescription: AppCompatTextView

    private lateinit var progressGroup: Group

    private lateinit var ivClose: AppCompatImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lifecycle.addObserver(ForegroundNfcHelper(requireActivity()))
        nfcModel = arguments?.getCorrectParcelable(MESSAGES_EXTRA, NfcModel::class.java)

        return inflater.inflate(
            R.layout.dialog_bottom_sheet,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    override fun onResume() {
        super.onResume()
        configureViews()
        messageWriter.setMessageListener(this)
        setCurrentState(nfcModel?.startState)
    }

    override fun onPause() {
        ivClose.setOnClickListener(null)
        messageWriter.setMessageListener(null)
        super.onPause()
    }

    override fun onProgress() {
        setCurrentState(nfcModel?.progressState)
    }

    override fun onSuccess() {
        CoroutineScope(Dispatchers.Main).launch {
            setCurrentState(nfcModel?.successState)
            delay(1000)
            dismiss()
        }
    }

    override fun onFailure(exception: Exception) {
        setCurrentState(
            nfcModel?.failureState
                ?.copy(exception = exception)
        )
    }

    fun onNewIntent(intent: Intent?) {
        if (intent?.action == NfcAdapter.ACTION_NDEF_DISCOVERED || intent?.action == NfcAdapter.ACTION_TECH_DISCOVERED) {
            val tag = getTagFromIntent(intent)

            if (tag != null && nfcModel != null) {
                messageWriter.writeMessagesToTag(nfcModel!!, tag)
            }
        }
    }

    private fun initViews() {
        val startLayout = requireView().findViewById<View>(R.id.start)
        val progressLayout = requireView().findViewById<View>(R.id.progress)
        val successLayout = requireView().findViewById<View>(R.id.success)
        val failureLayout = requireView().findViewById<View>(R.id.failure)

        startDescription = startLayout.findViewById(R.id.tv_description)
        successDescription = successLayout.findViewById(R.id.tv_description)
        progressDescription = progressLayout.findViewById(R.id.tv_progress_title)
        failureDescription = failureLayout.findViewById(R.id.tv_failure)

        progressGroup = progressLayout.findViewById(R.id.group_progress)

        ivClose = requireView().findViewById(R.id.iv_close)
    }

    private fun configureViews() {
        ivClose.setOnClickListener {
            dismiss()
        }
    }

    private fun setCurrentState(state: NfcSheetState?) {
        startDescription.isVisible = false
        progressGroup.isVisible = false
        successDescription.isVisible = false
        failureDescription.isVisible = false

        when (state) {
            is StartState -> {
                startDescription.isVisible = true
                startDescription.text = getString(state.title)
            }

            is ProgressState -> {
                progressDescription.text = getString(state.title)
                progressGroup.isVisible = true
            }

            is SuccessState -> {
                successDescription.text = getString(state.title)
                successDescription.isVisible = true
            }

            is FailureState -> {
                failureDescription.text = getString(state.title, state.exception.message)
                failureDescription.isVisible = true
            }

            else -> {
                startDescription.text = getString(R.string.title_something_goes_wrong)
                startDescription.isVisible = true
            }
        }
    }

    private fun getTagFromIntent(intent: Intent): Tag? =
        intent.getCorrectParcelableExtra(NfcAdapter.EXTRA_TAG, Tag::class.java)

}